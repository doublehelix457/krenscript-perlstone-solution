# README #

Have you heard of Perlstone? If you haven't, you're lucky. Perlstone is a GOD AWFUL language inside craftbook for the purpose of making programmable IC's. This language is caveman-like, irritating to understand and use, and extremely hard to learn. It is written on signs for the Programmable Logic IC [MC5000] and [MC5001]. Because of limited sign space, the language has to be extremely short, Very abbreviated, Very annoying. Nobody has bothered making a tutorial on Perlstone, and those who know it don't care to share. KRENScript is designed to be written in a signed book, interpreted by the Craftbook plugin, and then executed on the sign that it represents. This eliminates the need to have abbreviated nonsensical garbage and allows a more user friendly way of writing your own custom IC's for craftbook!!!


### What is this repository for? ###

* Annoyed with Perlstone? Cant find documentation? Here's a practical solution to writing your own Craftbook IC's.
* Version 1.0


### Who do I talk to? ###

send me an email at doublehelix457.dev@gmail.com! Make sure the subject contains: MC6000 so I get back to you quicker.
